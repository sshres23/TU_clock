#GitHub Reference: https://github.com/the-rasberry-pi-guy/lcd
# Importing necessary libraries for communication and display use
import drivers
from time import sleep
from datetime import datetime

# Loading driver for display
display = drivers.Lcd()

try:
    print("Writing to display")
    display.lcd_display_string("TU Clock", 1)  # Write line of text to first li$
    while True:

# Load the driver and set it to "display"
display = drivers.Lcd()

try:
    print("Writing to display")
    display.lcd_display_string("TU Clock", 1)  # Write line of text to first li$
    while True:
        # displaying the time
        display.lcd_display_string(str(datetime.now().time()), 2)
        
except KeyboardInterrupt:
    # Note press ctrl+c to exit the program. 
    print("Cleaning up!")
    display.lcd_clear()
